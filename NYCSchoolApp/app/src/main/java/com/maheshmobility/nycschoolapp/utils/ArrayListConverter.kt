package com.maheshmobility.nycschoolapp.utils

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


/**
 * A group of *ArrayListConverter*.
 *
 * This class would be used for converting *ArrayListConverter* to *String* and vice versa.
 *
 * @property fromStringArrayList function to convert string to arraylist.
 * @property toStringArrayList function to arraylist to string.
 * @constructor Sample Written to show how to write documentation.
 */
class ArrayListConverter {
    @TypeConverter
    fun fromStringArrayList(value: ArrayList<String>): String {

        return Gson().toJson(value)
    }

    @TypeConverter
    fun toStringArrayList(value: String): ArrayList<String> {
        return try {
            Gson().fromJson(value)
        } catch (e: Exception) {
            arrayListOf()
        }
    }
}

inline fun <reified T> Gson.fromJson(json: String) =
    fromJson<T>(json, object : TypeToken<T>() {}.type)