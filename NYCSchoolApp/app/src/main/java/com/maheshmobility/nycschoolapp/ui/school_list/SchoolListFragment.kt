package com.maheshmobility.nycschoolapp.ui.school_list

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.maheshmobility.nycschoolapp.R
import com.maheshmobility.nycschoolapp.databinding.FragmentSchoolListBinding
import com.maheshmobility.nycschoolapp.ui.adapter.SchoolListAdapter
import com.maheshmobility.nycschoolapp.ui.school_details.SchoolDetailsFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch


@AndroidEntryPoint
class SchoolListFragment : Fragment() {

    private var _binding: FragmentSchoolListBinding? = null

    private val binding get() = _binding!!
    private val viewModel: SchoolListViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSchoolListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.swipeRefresh.setOnRefreshListener {
            viewModel.fetchSchoolDetails()
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.uiState.collect { schoolListUiState ->
                when (schoolListUiState) {
                    is SchoolListUiState.Success -> {
                        binding.recyclerViewSchoolList.layoutManager = LinearLayoutManager(
                            requireContext(),
                            LinearLayoutManager.VERTICAL,
                            false
                        )
                        binding.recyclerViewSchoolList.adapter = SchoolListAdapter(
                            clickAction = { schoolItemUiState, state ->
                                //TODO: Should be done by Enum or Sealed class
                                when (state) {
                                    1 -> {
                                        val detailsFragment = SchoolDetailsFragment()
                                        val bundle = Bundle()
                                        bundle.putString("schoolItem", schoolItemUiState.dbn)
                                        detailsFragment.arguments = bundle
                                        requireActivity().supportFragmentManager.beginTransaction()
                                            .replace(
                                                R.id.fragment_container_view,
                                                detailsFragment
                                            )
                                            .addToBackStack(null)
                                            .commit()
                                    }

                                    2 -> {

                                    }

                                    3 -> {
                                    }

                                    4 -> {
                                    }

                                    5 -> {
                                        val gmmIntentUri =
                                            Uri.parse("google.streetview:cbll=${schoolItemUiState.latLong}")
                                        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                                        mapIntent.setPackage("com.google.android.apps.maps")
                                        startActivity(mapIntent)
                                    }

                                    else -> {
                                    }
                                }


                            },
                            satScoresList = schoolListUiState.schoolItems
                        )
                        if (binding.swipeRefresh.isRefreshing) binding.swipeRefresh.isRefreshing =
                            false
                    }

                    is SchoolListUiState.Error -> {
                        if (binding.swipeRefresh.isRefreshing) binding.swipeRefresh.isRefreshing =
                            false
                    }
                    is SchoolListUiState.Loading -> {}
                    is SchoolListUiState.Empty -> {
                    }
                    else -> {}
                }
            }
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}