package com.maheshmobility.nycschoolapp.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.maheshmobility.nycschoolapp.data.database.entity.SchoolEntity

@Database(entities = [SchoolEntity::class], version = 1, exportSchema = false)
abstract class SchoolDatabase : RoomDatabase() {
    abstract fun schoolDao(): SchoolDao
}