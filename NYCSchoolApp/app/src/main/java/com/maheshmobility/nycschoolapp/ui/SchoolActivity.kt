package com.maheshmobility.nycschoolapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.maheshmobility.nycschoolapp.R
import com.maheshmobility.nycschoolapp.databinding.ActivitySchoolBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySchoolBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySchoolBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        binding.toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.teal_700))
        binding.toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white))
    }
}