package com.maheshmobility.nycschoolapp.data.network

import com.maheshmobility.nycschoolapp.data.model.School
import com.maheshmobility.nycschoolapp.data.model.Stats
import com.maheshmobility.nycschoolapp.utils.Results
import javax.inject.Inject

class SchoolRemoteDataSource @Inject constructor(private val schoolApiService: SchoolApiService) {

    suspend fun fetchSchoolList(): Results<List<School>> {
        return try {
            val result = schoolApiService.getSchoolList()
            if (result.isSuccessful && result.body() != null) {
                Results.Success(result.body()!!)
            } else {
                Results.Error(Exception("Error"))
            }
        } catch (e: Throwable) {
            Results.Error(e)
        }
    }

    suspend fun fetchSchoolSatDetails(dbn: String): Results<List<Stats>> {
        return try {
            val result = schoolApiService.getSchoolSatDetails(dbn)
            if (result.isSuccessful && result.body() != null) {
                Results.Success(result.body()!!)
            } else {
                Results.Error(Exception("Error"))
            }
        } catch (e: Throwable) {
            Results.Error(e)
        }
    }
}