package com.maheshmobility.nycschoolapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NYCApplication : Application() {

}