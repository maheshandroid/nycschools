package com.maheshmobility.nycschoolapp.data.model

data class GradientCombo( val color1: Int, val color2: Int)
