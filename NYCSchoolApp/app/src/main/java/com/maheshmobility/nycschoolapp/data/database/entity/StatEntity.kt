package com.maheshmobility.nycschoolapp.data.database.entity

import com.maheshmobility.nycschoolapp.data.model.Stats

data class StatEntity(
    val dbn: String,
    val numOfSatTestTakers: String,
    val satCriticalReadingAvgScore: String,
    val satMathAvgScore: String,
    val satWritingAvgScore: String
) {
    companion object {
        fun toEntity(items: List<Stats>): StatEntity {
            val item = items.first()
            return StatEntity(
                dbn = item.dbn,
                numOfSatTestTakers = item.numOfSatTestTakers ?: "",
                satCriticalReadingAvgScore = item.satCriticalReadingAvgScore ?: "",
                satMathAvgScore = item.satMathAvgScore ?: "",
                satWritingAvgScore = item.satWritingAvgScore ?: ""
            )
        }
    }
}

