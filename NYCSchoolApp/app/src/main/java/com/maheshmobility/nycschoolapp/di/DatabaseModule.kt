package com.maheshmobility.nycschoolapp.di

import android.content.Context
import androidx.room.Room
import com.maheshmobility.nycschoolapp.data.database.SchoolDao
import com.maheshmobility.nycschoolapp.data.database.SchoolDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {

    @Provides
    @Singleton
    fun provideSchoolDatabase(@ApplicationContext appContext: Context): SchoolDatabase {
        return Room.databaseBuilder(
            appContext,
            SchoolDatabase::class.java,
            "school.db"
        ).build()
    }

    @Provides
    fun provideSchoolDao(schoolDataBase: SchoolDatabase): SchoolDao {
        return schoolDataBase.schoolDao()
    }
}