package com.maheshmobility.nycschoolapp.di

import com.maheshmobility.nycschoolapp.data.database.SchoolDao
import com.maheshmobility.nycschoolapp.data.network.SchoolRemoteDataSource
import com.maheshmobility.nycschoolapp.data.repo.SchoolRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped

@Module
@InstallIn(ActivityRetainedComponent::class)
object RepositoryModule {

    @Provides
    @ActivityRetainedScoped
    fun provideSchoolListRepository(
        schoolRemoteDataSource: SchoolRemoteDataSource,
        schoolDao: SchoolDao
    ): SchoolRepository {
        return SchoolRepository(
            schoolRemoteDataSource = schoolRemoteDataSource,
            schoolDao = schoolDao
        )
    }
}