package com.maheshmobility.nycschoolapp.data.repo

import com.maheshmobility.nycschoolapp.data.database.entity.SchoolEntity
import android.util.Log
import com.maheshmobility.nycschoolapp.data.database.SchoolDao
import com.maheshmobility.nycschoolapp.data.database.entity.StatEntity
import com.maheshmobility.nycschoolapp.data.network.SchoolRemoteDataSource
import com.maheshmobility.nycschoolapp.utils.Results
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SchoolRepository @Inject constructor(
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO,
    private val schoolRemoteDataSource: SchoolRemoteDataSource,
    private val schoolDao: SchoolDao
) {

    suspend fun getSchoolList(): Flow<List<SchoolEntity>> {
        return withContext(dispatcher) {
            schoolDao.getAllSchool().distinctUntilChanged()
        }
    }

    suspend fun fetchSchoolList() {
        return withContext(dispatcher) {
            when (val schoolList = schoolRemoteDataSource.fetchSchoolList()) {
                is Results.Error -> {
                    Log.e(TAG, "${schoolList.throwable.message}")
                }
                is Results.Success -> {
                    schoolDao.nukeTable()
                    schoolDao.insertAll(SchoolEntity.toEntity(schoolList.data))
                }
            }
        }
    }

    suspend fun getSchoolSatDetails(dbn: String): Flow<SchoolEntity> {
        return withContext(dispatcher) {
            schoolDao.getSchoolEntity(dbn).distinctUntilChanged()
        }
    }

    suspend fun fetchSchoolSatDetails(dbn: String) {
        return withContext(dispatcher) {
            when (val statResults = schoolRemoteDataSource.fetchSchoolSatDetails(dbn)) {
                is Results.Error -> {
                    Log.e(TAG, "${statResults.throwable.message}")
                }
                is Results.Success -> {
                    if (statResults.data.isEmpty().not()) {
                        schoolDao.update(StatEntity.toEntity(statResults.data))
                    }
                }
            }
        }
    }

    companion object {
        const val TAG = "SchoolListRepository"
    }
}