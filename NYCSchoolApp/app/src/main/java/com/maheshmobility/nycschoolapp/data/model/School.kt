package com.maheshmobility.nycschoolapp.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class School(
    val dbn: String,
    @SerializedName("school_name")
    val schoolName: String?,
    val boro: String,
    @SerializedName("overview_paragraph")
    val overviewParagraph: String?,
    @SerializedName("school_10th_seats")
    val school10thSeats: String,
    @SerializedName("academicopportunities1") val academicOpportunities1: String?,
    @SerializedName("academicopportunities2") val academicOpportunities2: String?,
    @SerializedName("academicopportunities3") val academicOpportunities3: String?,
    @SerializedName("ell_programs")
    val ellPrograms: String,
    val neighborhood: String?,
    @SerializedName("building_code")
    val buildingCode: String,
    val location: String?,
    @SerializedName("phone_number")
    val phoneNumber: String?,
    @SerializedName("fax_number")
    val faxNumber: String,
    @SerializedName("school_email")
    val schoolEmail: String?,
    val website: String?,
    val subway: String?,
    val bus: String?,
    val grades2018: String,
    val finalgrades: String,
    @SerializedName("total_students")
    val totalStudents: String,
    @SerializedName("extracurricular_activities")
    val extracurricularActivities: String,
    @SerializedName("school_sports")
    val schoolSports: String,
    @SerializedName("attendance_rate")
    val attendanceRate: String,
    @SerializedName("pct_stu_enough_variety")
    val pctStuEnoughVariety: String,
    @SerializedName("pct_stu_safe")
    val pctStuSafe: String,
    @SerializedName("school_accessibility_description")
    val schoolAccessibilityDescription: String?,
    val directions1: String,
    @SerializedName("requirement1_1")
    val requirement11: String?,
    @SerializedName("requirement2_1")
    val requirement21: String?,
    @SerializedName("requirement3_1")
    val requirement31: String?,
    @SerializedName("requirement4_1")
    val requirement41: String?,
    @SerializedName("requirement5_1")
    val requirement51: String?,
    @SerializedName("offer_rate1")
    val offerRate1: String?,
    val program1: String,
    val code1: String,
    val interest1: String,
    val method1: String,
    val seats9ge1: String,
    val grade9gefilledflag1: String,
    val grade9geapplicants1: String,
    val seats9swd1: String,
    val grade9swdfilledflag1: String,
    val grade9swdapplicants1: String,
    val seats101: String,
    val admissionspriority11: String?,
    val admissionspriority21: String?,
    val admissionspriority31: String?,
    val grade9geapplicantsperseat1: String,
    val grade9swdapplicantsperseat1: String,
    @SerializedName("primary_address_line_1")
    val primaryAddressLine1: String?,
    val city: String?,
    val zip: String?,
    @SerializedName("state_code")
    val stateCode: String,
    val latitude: String?,
    val longitude: String?,
    @SerializedName("community_board")
    val communityBoard: String,
    @SerializedName("council_district")
    val councilDistrict: String,
    @SerializedName("census_tract")
    val censusTract: String,
    val bin: String,
    val bbl: String,
    val nta: String,
    val borough: String,
    @SerializedName("language_classes")
    val languageClasses: String?,
    @SerializedName("addtl_info1")
    val addtlInfo1: String?,
    val transfer: String?,
    val eligibility1: String?,
): Parcelable

