package com.maheshmobility.nycschoolapp.data.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.maheshmobility.nycschoolapp.data.model.School

@Entity(tableName = SchoolEntity.TABLE_NAME_SCHOOL)
data class SchoolEntity(
    @PrimaryKey(autoGenerate = false)
    val dbn: String,
    val primaryAddressLine1: String,
    val overviewParagraph: String,
    val phoneNumber: String,
    val schoolEmail: String,
    val schoolName: String,
    val website: String,
    val zip: String,
    val city: String,
    val academicOpportunitiesSecond: String,
    val academicOpportunitiesFirst: String,
    val neighborhood: String,
    val location: String,
    val subway: String,
    val bus: String,
    val latitude: String,
    val longitude: String,
    val numOfSatTestTakers: String,
    val satCriticalReadingAvgScore: String,
    val satMathAvgScore: String,
    val satWritingAvgScore: String
) {
    companion object {
        const val TABLE_NAME_SCHOOL = "NycSchool"

        fun toEntity(responseList: List<School>): List<SchoolEntity> {
            return responseList.fold(emptyList()) { acc, item ->
                val schoolEntity = if (item.dbn != null) SchoolEntity(
                    dbn = item.dbn,
                    primaryAddressLine1 = item.primaryAddressLine1 ?: "",
                    overviewParagraph = item.overviewParagraph ?: "",
                    phoneNumber = item.phoneNumber ?: "",
                    schoolEmail = item.schoolEmail ?: "",
                    schoolName = item.schoolName ?: "",
                    website = item.website ?: "",
                    zip = item.zip ?: "",
                    city = item.city ?: "",
                    academicOpportunitiesSecond = item.academicOpportunities1 ?: "",
                    academicOpportunitiesFirst = item.academicOpportunities2 ?: "",
                    neighborhood = item.neighborhood ?: "",
                    location = item.location ?: "",
                    subway = item.subway ?: "",
                    bus = item.bus ?: "",
                    latitude = item.latitude ?: "",
                    longitude = item.longitude ?: "",
                    numOfSatTestTakers = "",
                    satCriticalReadingAvgScore = "",
                    satMathAvgScore = "",
                    satWritingAvgScore = "",
                ) else null
                acc.plus(schoolEntity).filterNotNull()
            }
        }
    }
}

