package com.maheshmobility.nycschoolapp.ui.adapter

import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.graphics.toColorInt
import androidx.recyclerview.widget.RecyclerView
import com.maheshmobility.nycschoolapp.data.model.GradientCombo
import com.maheshmobility.nycschoolapp.databinding.ListItemSchoolItemBinding
import com.maheshmobility.nycschoolapp.ui.school_list.SchoolItemUiState
import kotlin.random.Random


class SchoolListAdapter constructor(
    private val clickAction: (SchoolItemUiState, state: Int) -> Unit,
    private val satScoresList: List<SchoolItemUiState>
) : RecyclerView.Adapter<SchoolListAdapter.SchoolListViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SchoolListViewHolder {
        val binding =
            ListItemSchoolItemBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
        return SchoolListViewHolder(binding)
    }

    fun submitList(schoolItems: List<SchoolItemUiState>) {
        satScoresList.toMutableList().addAll(schoolItems)
        notifyItemRangeChanged(0, schoolItems.size)
    }

    override fun onBindViewHolder(holder: SchoolListViewHolder, position: Int) {
        val item = satScoresList[position]
        holder.setDetail(item)
    }

    override fun getItemCount(): Int {
        return satScoresList.size
    }


    inner class SchoolListViewHolder(private val binding: ListItemSchoolItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            binding.cardBackground.setOnClickListener {
                clickAction(satScoresList[adapterPosition], 1)
            }
            binding.email.setOnClickListener {
                clickAction(satScoresList[adapterPosition], 2)
            }
            binding.phone.setOnClickListener {
                clickAction(satScoresList[adapterPosition], 3)
            }
            binding.website.setOnClickListener {
                clickAction(satScoresList[adapterPosition], 4)
            }
            binding.address.setOnClickListener {
                clickAction(satScoresList[adapterPosition], 5)
            }
        }

        fun setDetail(schoolItemUiState: SchoolItemUiState) {
            with(schoolItemUiState) {
                binding.schoolName.text = name
                binding.email.text = email
                binding.phone.text = phoneNumber
                binding.website.text = website
                binding.address.text = primaryAddressLine1

                if (primaryAddressLine1.isEmpty()) {
                    binding.address.visibility = android.view.View.GONE
                }
                if (email.isEmpty()) {
                    binding.email.visibility = android.view.View.GONE
                }
                if (phoneNumber.isEmpty()) {
                    binding.phone.visibility = android.view.View.GONE
                }
                if (website.isEmpty()) {
                    binding.website.visibility = android.view.View.GONE
                }

                val gd = GradientDrawable(
                    GradientDrawable.Orientation.BL_TR,
                    intArrayOf(randomColours().color1, randomColours().color2)
                )
                gd.gradientType = GradientDrawable.LINEAR_GRADIENT
                gd.cornerRadius = 0f
                binding.cardConstraint.background = gd
            }
        }


        private fun randomColours(): GradientCombo {
            val gradientCombos = (mutableListOf<GradientCombo>())
            gradientCombos.add(GradientCombo("#FFD3A5".toColorInt(), "#FD6585".toColorInt()))
            gradientCombos.add(GradientCombo("#0F3443".toColorInt(), "#34E89E".toColorInt()))
            gradientCombos.add(GradientCombo("#12C2E9".toColorInt(), "#F64F59".toColorInt()))
            gradientCombos.add(GradientCombo("#FCCF31".toColorInt(), "#F55555".toColorInt()))
            gradientCombos.add(GradientCombo("#7F7FD5".toColorInt(), "#91EAE4".toColorInt()))
            gradientCombos.add(GradientCombo("#3E5151".toColorInt(), "#DECBA4".toColorInt()))
            gradientCombos.add(GradientCombo("#333399".toColorInt(), "#FF00CC".toColorInt()))
            gradientCombos.add(GradientCombo("#31B7C2".toColorInt(), "#7BC393".toColorInt()))
            gradientCombos.add(GradientCombo("#2E3192".toColorInt(), "#1BFFFF".toColorInt()))
            gradientCombos.add(GradientCombo("#D4145A".toColorInt(), "#FBB03B".toColorInt()))
            gradientCombos.add(GradientCombo("#009245".toColorInt(), "#FCEE21".toColorInt()))
            gradientCombos.add(GradientCombo("#662D8C".toColorInt(), "#ED1E79".toColorInt()))
            gradientCombos.add(GradientCombo("#EE9CA7".toColorInt(), "#FFDDE1".toColorInt()))
            gradientCombos.add(GradientCombo("#614385".toColorInt(), "#516395".toColorInt()))
            val randomValues = List(1) { Random.nextInt(0, gradientCombos.size) }
            return gradientCombos[randomValues[0]]
        }

    }
}
