package com.maheshmobility.nycschoolapp.ui

interface SchoolListAction {
    fun onSwiped()

    fun onItemClicked(position: Int)
}