package com.maheshmobility.nycschoolapp.ui.school_details

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.maheshmobility.nycschoolapp.R
import com.maheshmobility.nycschoolapp.databinding.FragmentSchoolDetailsBinding
import com.maheshmobility.nycschoolapp.ui.school_list.SchoolItemUiState
import com.maheshmobility.nycschoolapp.ui.school_list.SchoolListUiState
import com.maheshmobility.nycschoolapp.ui.school_list.SchoolListViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SchoolDetailsFragment : Fragment() {
    private var _binding: FragmentSchoolDetailsBinding? = null
    private val binding get() = _binding!!
    private val viewModel: SchoolDetailViewModel by viewModels()
    private var schoolItem: String? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        schoolItem = requireArguments().getString("schoolItem")
        _binding = FragmentSchoolDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        schoolItem?.let {
            viewModel.getSchoolDetails(it)
        }
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.uiState.collect { schoolDetailScreenUiState ->
                when (schoolDetailScreenUiState) {
                    is SchoolDetailScreenUiState.Success -> {
                        loadData(schoolDetailScreenUiState.schoolDetails)
                        Log.d(
                            "SchoolDetailFragment",
                            "Success ${schoolDetailScreenUiState.schoolDetails.satMathAvgScore}"
                        )
                    }

                    is SchoolDetailScreenUiState.Error -> {
                        Log.e(
                            "SchoolDetailFragment",
                            "Error: ${schoolDetailScreenUiState.exception.message}"
                        )
                    }

                    is SchoolDetailScreenUiState.Loading -> {
                        Log.d("SchoolDetailFragment", "Loading")
                    }
                }
            }
        }
    }


    private fun loadData(schoolDetailUiState: SchoolDetailUiState) {
        binding.collegeName.text = schoolDetailUiState.name
        binding.collegeDescription.text = schoolDetailUiState.overviewParagraph

        binding.collegeOpportunity1.text = schoolDetailUiState.academicOpportunitiesFirst
        binding.collegeOpportunity2.text = schoolDetailUiState.academicOpportunitiesSecond
        binding.collegeOpportunity.text = getString(R.string.academic_opp)

        binding.testDetails.text = getString(R.string.test_details)
        binding.testTakers.text = "Number of SAT Test Takers: ${schoolDetailUiState.numOfSatTestTakers}"
        binding.criticalReadingScore.text = "SAT Critical Reading Score: ${schoolDetailUiState.satCriticalReadingAvgScore}"
        binding.satWritingScore.text = "SAT Writing Score: ${schoolDetailUiState.satWritingAvgScore}"
        binding.satMathScore.text = "SAT Math Score: ${schoolDetailUiState.satMathAvgScore}"

        if(schoolDetailUiState.numOfSatTestTakers.isEmpty()){
            binding.collegeEducationCard.visibility = View.GONE
        }

        binding.collegeLocationCard.setOnClickListener {
            val gmmIntentUri =
                Uri.parse("google.streetview:cbll=${schoolDetailUiState.latlong}")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }
        binding.collegeLocation.text = schoolDetailUiState.location
        binding.collegeCity.text = "${schoolDetailUiState.city},${schoolDetailUiState.zip}"
        binding.collegeBus.text = "Bus: ${schoolDetailUiState.bus}"
        binding.collegeSubway.text = "Subway: ${schoolDetailUiState.subway}"
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}